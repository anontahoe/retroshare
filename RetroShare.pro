!include("retroshare.pri"): error("Could not include file retroshare.pri")

TEMPLATE = subdirs

SUBDIRS += \
		openpgpsdk \
		libbitdht \
		libretroshare \
		libresapi


message("Config flags: $$CONFIG")

# enable gui by default for now:
!--disable-gui:!--enable-gui {
	CONFIG += --enable-gui
	message("You should explicitly enable retroshare-gui using qmake \"CONFIG+=\--enable-gui\"")
}

--enable-gui {
	SUBDIRS += retroshare_gui
	message("enabling gui")
}

--enable-nogui {
	SUBDIRS += retroshare_nogui
	message("enabling nogui")
}

--enable-plugins {
	SUBDIRS += plugins
	message("enabling plugins")
}

--enable-nsa-patches {
	# those patches probably aren't literally made by the NSA
	# disabling the patches doesn't remove their code but makes it harmless
	# the actual disabling is done in retroshare.pri because of how DEFINES work
	message("enabling NSA patches")
} else {
	message("disabling NSA patches")
}

openpgpsdk.file = openpgpsdk/src/openpgpsdk.pro

libbitdht.file = libbitdht/src/libbitdht.pro

libretroshare.file = libretroshare/src/libretroshare.pro
libretroshare.depends = openpgpsdk libbitdht

libresapi.file = libresapi/src/libresapi.pro
libresapi.depends = libretroshare

retroshare_gui.file = retroshare-gui/src/retroshare-gui.pro
retroshare_gui.depends = libretroshare libresapi
retroshare_gui.target = retroshare-gui

retroshare_nogui.file = retroshare-nogui/src/retroshare-nogui.pro
retroshare_nogui.depends = libretroshare libresapi
retroshare_nogui.target = retroshare-nogui

plugins.file = plugins/plugins.pro
plugins.depends = retroshare_gui
plugins.target = plugins

wikipoos {
	SUBDIRS += pegmarkdown
	pegmarkdown.file = supportlibs/pegmarkdown/pegmarkdown.pro
	retroshare_gui.depends += pegmarkdown
}

tests {
	SUBDIRS += librssimulator
	librssimulator.file = tests/librssimulator/librssimulator.pro

	SUBDIRS += unittests
	unittests.file = tests/unittests/unittests.pro
	unittests.depends = libretroshare librssimulator
}
